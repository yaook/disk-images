#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -eux
export DIB_RELEASE="jammy"
# if you want to burn in a user + sshkey
# export DIB_DEV_USER_USERNAME="username"
# export DIB_DEV_USER_PWDLESS_SUDO="yes"
# export DIB_DEV_USER_AUTHORIZED_KEYS="./authorized_keys"

wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.squashfs.manifest
mkdir -p /root/.cache/image-create/
mv jammy-server-cloudimg-amd64.squashfs.manifest /root/.cache/image-create/

disk-image-create --no-tmpfs -o ubuntu22 vm cloud-init-nocloud block-device-efi growroot devuser ubuntu

