#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -eux
export DIB_RELEASE=noble

export ELEMENTS_PATH="$ELEMENTS_PATH":"$(dirname $BASH_SOURCE)/../dib-elements"
export DIB_REPOLOCATION_ironic_python_agent=https://github.com/cloudandheat/ironic-python-agent.git
export DIB_DEV_USER_USERNAME=ubuntu
export DIB_DEV_USER_PASSWORD=ubuntu
export DIB_DEV_USER_PWDLESS_SUDO=yes

disk-image-create -o ipa ironic-python-agent-ramdisk ubuntu devuser openssh-server mdadm-kill
