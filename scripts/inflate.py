#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

'''Script to online-expand a software RAID backed root filesystem onto further
disks, grow it to use all available disk space, and make sure all of the disks
are bootable.

It is intended to be used in the installation of baremetal nodes from images
that include mdraid metadata and LVM or LUKS volumes, features that might not
be fully supported by MaaS services such as Ironic.'''

import json
import os
import os.path
import re
import time
import sys

from subprocess import Popen, PIPE


def cmd(command, returns=(0,)):
    '''Runs a command, checks the return code and returns stdout.'''

    proc = Popen(command, stdout=PIPE, stderr=PIPE)
    stdout, _ = proc.communicate()
    if proc.returncode not in returns:
        raise Exception(f'Command {command} returned {proc.returncode},'
                        f'\nSTDOUT: {proc.stdout.read()}'
                        f'\nSTDERR: {proc.stderr.read()}')

    return stdout.decode('utf-8')


class BlockDevice():
    '''Represents a block device and its children (partitions, LVs, etc.)'''

    def __init__(self, lsblk_json, parent=None):
        self.type = lsblk_json['type']
        self.parttype = lsblk_json['parttype']
        self.fstype = lsblk_json['fstype']
        self.path = lsblk_json['name']
        self.size = lsblk_json['size']

        self.mountpoint = lsblk_json['mountpoint']

        self.partnum = self._get_partnum()
        self.parent = parent

        self.children = []
        if 'children' in lsblk_json:
            self.children = [BlockDevice(child, self)
                             for child in lsblk_json['children']]

    def _get_partnum(self):
        '''Get the partition number of the device, as required by growpart.'''

        udevadm = cmd(['udevadm', 'info', '--query', 'all', '--name',
                       self.path])
        match = re.search('^E: ID_PART_ENTRY_NUMBER=(\\d+)$', udevadm,
                          re.MULTILINE)
        return int(match.group(1)) if match else None

    def _update(self):
        '''Reload information on this device and its children, e.g. after
        changing the partitions of a disk.'''

        # wait required for lsblk to correctly detect partition type changes
        time.sleep(1)
        lsblk = cmd(['lsblk', '--json', '--path', '--output-all', self.path])
        self.__init__(json.loads(lsblk)['blockdevices'][0], self.parent)

    def get_mountpoints(self):
        '''Recursively get mountpoints of this device and its children.'''

        mps = [self.mountpoint] if self.mountpoint is not None else []
        for child in self.children:
            mps.extend(child.get_mountpoints())
        return mps

    def get_keyfile(self):
        '''Try to get the LUKS keyfile from crypttab.'''

        if self.type != 'crypt':
            raise Exception(f'{self.path} is not a crypt device.')

        name = os.path.basename(self.path)
        with open('/etc/crypttab', 'r') as crypttab:
            match = re.search(f'^{name}\\s+\\S+\\s+(\\S+)', crypttab.read(),
                              re.MULTILINE)
            if match and match.group(1) != 'none':
                return match.group(1)

        return None

    def clone(self, source):
        '''Copy children from another device, join RAID partitions.'''

        assert self.type == source.type
        assert self.parttype == source.parttype
        assert self.partnum == source.partnum

        if source.fstype == 'linux_raid_member':
            assert len(self.children) == 0
            assert len(source.children) == 1
            [raid] = source.children

            # we only support raid 1 at the moment
            assert raid.type == 'raid1'

            raid_info = cmd(['mdadm', '--detail', raid.path])
            match = re.search('^ +Raid Devices : (\\d+)$', raid_info,
                              re.MULTILINE)
            devices = int(match.group(1))

            cmd(['mdadm', '--grow', raid.path,
                 '--level=1',
                 f'--raid-devices={devices + 1}',
                 '--add', self.path])

        elif source.type == 'part':
            cmd(['dd', 'bs=512', f'if={source.path}', f'of={self.path}'])

        elif source.type == 'disk':
            # disable mdraid autoassembly in case a new partition is created
            # at the exact location of a previous raid partition
            if os.path.isfile('/etc/mdadm.conf'):
                os.replace('/etc/mdadm.conf', '/etc/mdadm.conf.backup')
            with open('/etc/mdadm.conf', 'w') as mdadm_conf:
                mdadm_conf.write('DEVICE /dev/null\n')
                mdadm_conf.close()

            # copy the GPT
            cmd(['sgdisk', source.path, '-R', self.path])
            cmd(['sgdisk', self.path, '-G'])
            # copy the MBR, in case we are on a legacy boot system
            cmd(['dd', 'bs=512', f'if={source.path}', f'of={self.path}',
                 'count=1'])
            self._update()

            self_parts = sorted(self.children, key=lambda c: c.partnum)
            source_parts = sorted(source.children, key=lambda c: c.partnum)
            for self_part, source_part in zip(self_parts, source_parts):
                self_part.clone(source_part)

            # once all partitions are cloned we can restore the previous
            # mdadm.conf (or lack thereof)
            if os.path.isfile('/etc/mdadm.conf.backup'):
                os.replace('/etc/mdadm.conf.backup', '/etc/mdadm.conf')
            else:
                os.remove('/etc/mdadm.conf')

            self._update()

        else:
            raise Exception('Unsupported blockdevice type for cloning:'
                            f' {source.type} ({source.path})')

    def grow(self):
        '''Recursively grow the root filesystem and the devices backing it
        to use all available space.'''

        # TODO: this should be expanded to a configurable set of mountpoints
        # for LVM with respective absolute/relative size requirements
        if '/' not in self.get_mountpoints():
            return

        previous_size = self.size

        if self.type == 'disk':
            pass

        elif self.type in ['part', 'md']:
            cmd(['growpart', self.parent.path, str(self.partnum)],
                returns=(0, 1))

        elif self.type == 'raid1':
            cmd(['mdadm', '--grow', self.path, "--size=max"])

        elif self.type == 'crypt':
            keyfile = self.get_keyfile()
            kfopt = [f'--key-file={keyfile}'] if keyfile is not None else []
            cmd(['cryptsetup', 'resize', self.path] + kfopt)

        elif self.type == 'lvm':
            # TODO: get configured size mapped to mountpoint
            cmd(['lvextend', "--size", "20G", self.path, self.parent.path], returns=(0, 5))

        else:
            raise Exception('Unsupported blockdevice type for growing:'
                            f' {self.type} ({self.path})')

        if self.fstype == 'LVM2_member':
            cmd(['pvresize', self.path])

        if self.mountpoint == '/':
            # TODO: use fsadm
            if self.fstype in ['ext2', 'ext3', 'ext4']:
                cmd(['resize2fs', self.path])
            else:
                raise Exception('Unsupported filesystem for growing:'
                                f' {self.fstype} ({self.path})')

        self._update()

        if self.size == previous_size and self.type != 'disk':
            # no need to update the children if size didn't change
            return

        for child in self.children:
            child.grow()

    def reencrypt(self):
        '''Look recursively for crypt devices and trigger a reencryption.'''

        if self.type == 'crypt':
            keyfile = self.get_keyfile()
            kfopt = [f'--key-file={keyfile}'] if keyfile is not None else []
            cmd(['cryptsetup', 'reencrypt', self.parent.path] + kfopt)

        for child in self.children:
            child.reencrypt()


def inflate():
    '''Main function. Searches for bootdisk, expand software raid and boot
    partitions onto a secondary disk and grow the root filesystem to use
    the available space.'''

    bootdisk = None
    candidates = []

    lsblk = cmd(['lsblk', '--json', '--path', '--output-all'])

    for bdev in json.loads(lsblk)['blockdevices']:
        if bdev['type'] == 'disk' and bdev['rm'] in [False, '0']:
            disk = BlockDevice(bdev)
            if '/' in disk.get_mountpoints():
                if bootdisk is not None:
                    print("Image expansion seems to have been done already:",
                          f" ({bootdisk.path} and {disk.path} are both backing",
                          " the rootfs)", file=sys.stderr)
                    print("Nothing to do.")
                    return
                bootdisk = disk
            else:
                candidates.append(disk)

    candidates = [disk for disk in candidates if disk.size == bootdisk.size]

    if len(candidates) < 1:
        print("Only one suitable disk found: disabling RAID1 expansion")
        target = None
    else:
        target = candidates[0]

    print("Initiating reencryption")
    bootdisk.reencrypt()
    if target is not None:
        print("Growing RAID1")
        target.clone(bootdisk)
    print("Expanding partitions on source disk")
    bootdisk.grow()
    if target is not None:
        print("Expanding partitions on target disk")
        target.grow()


if __name__ == '__main__':
    inflate()
